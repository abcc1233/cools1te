(function() {
    var l = document.querySelector("link[rel*='icon']") || document.createElement('link');
    l.type = 'image/x-icon';
    l.rel = 'shortcut icon';
    l.href = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAB8klEQVR4Ae2UA49kQRSF3x9aG8HacTZY27YZrY0JxrZt27Zt8847ndykUemuMU/yVMj56tyqp0Ar75XeWnW3pF29aJau8pV3S18pEF7QOBfXyrvFtxXQzBEArnYA0FxeixPg6KequQN4Yd9IUHJJ39wAeCZ1EWsZYEYADryrIJvIdk2Na9uGNU+YnvtdM/MANlHtZExh2T0AmhkAz+Qu0havvqB2gISSANj5vEAO4G9gq47xud+1umV5X8Eg0gB7XhXQtZ9ZpEjUXNscZsJx25+VAUIKYOvjIpjLAeDHwsK7sbGAMwWw8WExnfuaLQ+AHc+CganxBTUDRgFOf2ZzBpA/19g0iG/S/4Ej7/JhKg8As4jsdmLdN8vFJESoRikGER1Djh5zpQEOvCmgS9+zySWqlljf3Ep5IvqEaXT1jQoBAC0NoB0VVs0qqunhdoZAEsLjComjNwGAOusPCstoJlZ8XhuXQrsc+uYQJ4lxcgCoE1alPwiGrV2DxGrpHKT4/DbyTWzQwJU39JNAbC4HsO6+1vkUXC8sCqi6mY3EalXB8qt6iIV50gB8Pk1dVsHVmr2gLYAhDSSFfgjJSAFg5cc/5uBjTi6FY58zANyWOkBm+5wB/MgsV9Tb7bkCuP4z87UC4eWqSjOLK2+/9ivzNrzHAZ9vEATY/UnzAAAAAElFTkSuQmCC';
    document.getElementsByTagName('head')[0].appendChild(l);
    document.title = 'Desmos | Testing';
})();